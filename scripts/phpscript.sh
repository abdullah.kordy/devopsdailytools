set -eux
# Turn on maintenance mode
php artisan down

# Install/update composer dependecies
composer update --no-interaction --prefer-dist --optimize-autoloader --no-dev

# Run database migrations
php artisan migrate --force

# Run database seeder
php artisan db:seed --class=PermissionSeeder --force
php artisan db:seed --class=AffiliateCategorySeeder --force

# Clear caches
php artisan cache:clear

# Clear expired password reset tokens
php artisan auth:clear-resets

# Clear and cache config
php artisan config:cache
php artisan config:clear

php artisan view:clear
# Install node modules
npm install || true

# Build assets using Laravel Mix
npm run prod || true

# Turn off maintenance mode
php artisan up

echo -e ' \033[0;32m✔ Done\033[0m'

